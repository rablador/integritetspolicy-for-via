## Integritetspolicy för Via

#### Allmänt
Via är en app för och av resenärer och tjänar inget syfte att lagra användardata i onödan. Den data som Via själv sparar (favoriter, prioriterade hållplatser etc.) lämnar aldrig din telefon. Den data som skickas till Västtrafik och Firebase (se nedan) är helt anonymiserad och kan aldrig användas för att indentifiera en verklig person. Om du skulle välja att avinstallera Via försvinner alla band mellan oss och dig och din telefon, även om du skulle välja att senare börja använda Via igen. Du blir en helt ny användare!

#### Västtrafik
Via använder sig av Västtrafiks officiella API:er för att visa upp aktuell trafikinformation. Det är alltså samma (och troligtvis betydligt mindre) information som du lämnar till Västtrafik när du använder deras officiella appar. Du kan läsa mer om Västtrafiks integritetspolicy [här](https://www.vasttrafik.se/om-vasttrafik/integritetspolicy/).

#### Firebase
Via använder sig av analysverktyget Firebase för att fånga upp eventuella fel och krascher som kan uppstå i appen. Firebase ger oss också grundläggande information om hur appen används. Den här informationen är viktig för att vi ska kunna förbättra och vidareutveckla Via på ett effektivt sätt. Du kan läsa mer om Firebase [här](https://firebase.google.com/).

#### Framtiden
I takt med att Via utvecklas och blir bättre kan det komma att uppstå nya omständigheter då vi behöver spara data som vi inte har behövt spara tidigare. Vi kommer dock aldrig att göra den typen av förändringar utan att samtidigt meddela dig, och den senaste informationen finns alltid tillgänglig här. Den här sidan är dessutom versionshanterad, så du kan följa precis alla förändringar som görs i vår integritetspolicy.